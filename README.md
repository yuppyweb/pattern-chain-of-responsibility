# Chain of Responsibility

Example of the Chain of Responsibility pattern implementation

## How to add to a project

```
composer config repositories.yuppyweb git https://gitlab.com/yuppyweb/pattern-chain-of-responsibility.git
```
```
composer config minimum-stability dev
```
```
composer require yuppyweb/pattern-chain-of-responsibility
```

## How to use

```
require_once 'vendor/autoload.php';

use PatternChainOfResponsibility\Exception\IncorrectUserRequirementsException;
use PatternChainOfResponsibility\Requirements\UserActivity;
use PatternChainOfResponsibility\Requirements\UserPermissions;
use PatternChainOfResponsibility\Requirements\UserRole;
use PatternChainOfResponsibility\Requirements\UserStatus;
use PatternChainOfResponsibility\User;

$requirements = new UserStatus(['active', 'blocked']);
$requirements
    ->setRequirements(new UserRole(['admin', 'customer']))
    ->setRequirements(new UserPermissions(['read_comment', 'change_price']))
    ->setRequirements(new UserActivity());

$user = User::createAdmin()->setStatus('active')->setCurrentActivity(['read_comment' => 500]);

try {
    $user->requirementsCheck($requirements);
} catch (IncorrectUserRequirementsException $exception) {
    echo $exception->getMessage();
}
```
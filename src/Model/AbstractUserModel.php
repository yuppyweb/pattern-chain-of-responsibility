<?php

namespace PatternChainOfResponsibility\Model;

abstract class AbstractUserModel implements UserModelInterface
{
    protected string $status;

    protected array $currentActivity = [];

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): UserModelInterface
    {
        $this->status = $status;

        return $this;
    }

    public function getPermissions(): array
    {
        return array_keys($this->permissionsWithLimits);
    }

    public function getCurrentActivity(): array
    {
        return array_merge(
            array_fill_keys($this->getPermissions(), 0),
            $this->currentActivity
        );
    }

    public function setCurrentActivity(array $activity): UserModelInterface
    {
        $permissions = $this->getPermissions();

        foreach ($activity as $permission => $count) {
            if (in_array($permission, $permissions)) {
                $this->currentActivity[$permission] = $count;
            }
        }

        return $this;
    }

    public function getActivityLimits(): array
    {
        return $this->permissionsWithLimits;
    }
}
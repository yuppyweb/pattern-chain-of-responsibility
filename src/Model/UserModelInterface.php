<?php

namespace PatternChainOfResponsibility\Model;

interface UserModelInterface
{
    public function getStatus(): string;

    public function setStatus(string $status): self;

    public function getRole(): string;

    public function getPermissions(): array;

    public function getCurrentActivity(): array;

    public function setCurrentActivity(array $activity): self;

    public function getActivityLimits(): array;
}
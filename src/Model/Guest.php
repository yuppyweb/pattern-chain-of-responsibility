<?php

namespace PatternChainOfResponsibility\Model;

class Guest extends AbstractUserModel
{
    protected array $permissionsWithLimits = [
        'read_comment' => 1000,
    ];

    public function getRole(): string
    {
        return 'guest';
    }
}
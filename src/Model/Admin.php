<?php

namespace PatternChainOfResponsibility\Model;

class Admin extends AbstractUserModel
{
    protected array $permissionsWithLimits = [
        'read_comment' => 5000,
        'write_comment'=> 1000,
        'moderation_comment' => 500,
        'change_price' => 300,
        'buy_product' => 200,
    ];

    public function getRole(): string
    {
        return 'admin';
    }
}
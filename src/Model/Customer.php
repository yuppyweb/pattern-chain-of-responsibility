<?php

namespace PatternChainOfResponsibility\Model;

class Customer extends AbstractUserModel
{
    protected array $permissionsWithLimits = [
        'read_comment' => 3000,
        'write_comment'=> 500,
        'buy_product' => 100,
    ];

    public function getRole(): string
    {
        return 'customer';
    }
}
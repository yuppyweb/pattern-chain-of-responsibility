<?php

namespace PatternChainOfResponsibility;

use PatternChainOfResponsibility\Model\Admin;
use PatternChainOfResponsibility\Model\Customer;
use PatternChainOfResponsibility\Model\Guest;
use PatternChainOfResponsibility\Model\UserModelInterface;
use PatternChainOfResponsibility\Requirements\UserRequirementsInterface;

class User
{
    private UserModelInterface $user;

    public function __construct(UserModelInterface $user)
    {
        $this->user = $user;
    }

    public function setStatus(string $status): self
    {
        $this->user->setStatus($status);

        return $this;
    }

    public function setCurrentActivity(array $activity): self
    {
        $this->user->setCurrentActivity($activity);

        return $this;
    }

    public function requirementsCheck(UserRequirementsInterface $requirements): void
    {
        $requirements->check($this->user);
    }

    public static function createAdmin(): self
    {
        return new self(new Admin());
    }

    public static function createCustomer(): self
    {
        return new self(new Customer());
    }

    public static function createGuest(): self
    {
        return new self(new Guest());
    }
}
<?php

namespace PatternChainOfResponsibility\Requirements;

use PatternChainOfResponsibility\Model\UserModelInterface;

interface UserRequirementsInterface
{
    public function setRequirements(UserRequirementsInterface $requirements): UserRequirementsInterface;

    public function check(UserModelInterface $userModel): void;
}
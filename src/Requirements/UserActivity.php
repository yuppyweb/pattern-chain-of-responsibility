<?php

namespace PatternChainOfResponsibility\Requirements;

use PatternChainOfResponsibility\Exception\IncorrectUserActivityException;
use PatternChainOfResponsibility\Model\UserModelInterface;

class UserActivity extends AbstractUserRequirements
{
    /**
     * @throws IncorrectUserActivityException
     */
    public function check(UserModelInterface $userModel): void
    {
        $activityLimits = $userModel->getActivityLimits();

        foreach ($userModel->getCurrentActivity() as $permission => $count) {
            if ($count > $activityLimits[$permission]) {
                throw new IncorrectUserActivityException("Exceeded activity for '{$permission}'");
            }
        }

        parent::check($userModel);
    }
}
<?php

namespace PatternChainOfResponsibility\Requirements;

use PatternChainOfResponsibility\Exception\IncorrectUserStatusException;
use PatternChainOfResponsibility\Model\UserModelInterface;

class UserStatus extends AbstractUserRequirements
{
    private array $allowedStatuses;

    public function __construct(array $allowedStatuses)
    {
        $this->allowedStatuses = $allowedStatuses;
    }

    /**
     * @throws IncorrectUserStatusException
     */
    public function check(UserModelInterface $userModel): void
    {
        $status = $userModel->getStatus();

        if (!in_array($status, $this->allowedStatuses)) {
            throw new IncorrectUserStatusException("The status '{$status}' is not in the allowed");
        }

        parent::check($userModel);
    }
}
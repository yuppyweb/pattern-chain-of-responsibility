<?php

namespace PatternChainOfResponsibility\Requirements;

use PatternChainOfResponsibility\Exception\IncorrectUserPermissionsException;
use PatternChainOfResponsibility\Model\UserModelInterface;

class UserPermissions extends AbstractUserRequirements
{
    private array $requiredPermissions;

    public function __construct(array $requiredPermissions)
    {
        $this->requiredPermissions = $requiredPermissions;
    }

    /**
     * @throws IncorrectUserPermissionsException
     */
    public function check(UserModelInterface $userModel): void
    {
        $permissions = $userModel->getPermissions();

        foreach ($this->requiredPermissions as $permission) {
            if (!in_array($permission, $permissions)) {
                throw new IncorrectUserPermissionsException('There are no necessary permissions');
            }
        }

        parent::check($userModel);
    }
}
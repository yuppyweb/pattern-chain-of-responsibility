<?php

namespace PatternChainOfResponsibility\Requirements;

use PatternChainOfResponsibility\Model\UserModelInterface;

abstract class AbstractUserRequirements implements UserRequirementsInterface
{
    private ?UserRequirementsInterface $requirements = null;

    public function setRequirements(UserRequirementsInterface $requirements): UserRequirementsInterface
    {
        $this->requirements = $requirements;

        return $requirements;
    }

    public function check(UserModelInterface $userModel): void
    {
        $this->requirements?->check($userModel);
    }
}
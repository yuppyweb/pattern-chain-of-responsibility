<?php

namespace PatternChainOfResponsibility\Requirements;

use PatternChainOfResponsibility\Exception\IncorrectUserRoleException;
use PatternChainOfResponsibility\Model\UserModelInterface;

class UserRole extends AbstractUserRequirements
{
    private array $allowedRoles;

    public function __construct(array $allowedRoles)
    {
        $this->allowedRoles = $allowedRoles;
    }

    /**
     * @throws IncorrectUserRoleException
     */
    public function check(UserModelInterface $userModel): void
    {
        $role = $userModel->getRole();

        if (!in_array($role, $this->allowedRoles)) {
            throw new IncorrectUserRoleException("The '{$role}' user role is not in the allowed");
        }

        parent::check($userModel);
    }
}
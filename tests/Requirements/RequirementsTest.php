<?php

namespace PatternChainOfResponsibility\Test\Requirements;

use PatternChainOfResponsibility\Exception\IncorrectUserActivityException;
use PatternChainOfResponsibility\Exception\IncorrectUserPermissionsException;
use PatternChainOfResponsibility\Exception\IncorrectUserRoleException;
use PatternChainOfResponsibility\Exception\IncorrectUserStatusException;
use PatternChainOfResponsibility\Model\Admin;
use PatternChainOfResponsibility\Model\Customer;
use PatternChainOfResponsibility\Model\Guest;
use PatternChainOfResponsibility\Model\UserModelInterface;
use PatternChainOfResponsibility\Requirements\UserActivity;
use PatternChainOfResponsibility\Requirements\UserPermissions;
use PatternChainOfResponsibility\Requirements\UserRole;
use PatternChainOfResponsibility\Requirements\UserStatus;
use PatternChainOfResponsibility\User;
use PHPUnit\Framework\TestCase;

class RequirementsTest extends TestCase
{
    /**
     * @dataProvider requirementsDataProvider
     */
    public function testRequirements(
        UserModelInterface $userRole,
        string $status,
        array $currentActivity,
        array $allowedStatuses,
        array $allowedRoles,
        array $requiredPermissions,
        ?string $exception
    )
    {
        $user = new User($userRole);
        $user
            ->setStatus($status)
            ->setCurrentActivity($currentActivity);

        $requirements = new UserStatus($allowedStatuses);
        $requirements
            ->setRequirements(new UserRole($allowedRoles))
            ->setRequirements(new UserPermissions($requiredPermissions))
            ->setRequirements(new UserActivity());

        if ($exception) {
            $this->expectException($exception);
        }

        $user->requirementsCheck($requirements);

        if (!$exception) {
            $this->assertNull($exception);
        }
    }

    public static function requirementsDataProvider(): array
    {
       return [
           [
               new Admin(),
               'active',
               [],
               ['active'],
               ['admin'],
               ['moderation_comment'],
               null
           ],
           [
               new Customer(),
               'blocked',
               [],
               ['active'],
               ['customer'],
               [],
               IncorrectUserStatusException::class
           ],
           [
               new Guest(),
               'active',
               [],
               ['active'],
               ['admin'],
               [],
               IncorrectUserRoleException::class
           ],
           [
               new Guest(),
               'active',
               [],
               ['active'],
               ['guest'],
               ['write_comment'],
               IncorrectUserPermissionsException::class
           ],
           [
               new Customer(),
               'active',
               ['write_comment' => 501],
               ['active'],
               ['customer'],
               ['read_comment', 'write_comment'],
               IncorrectUserActivityException::class
           ],
           [
               new Guest(),
               'active',
               ['read_comment' => 1000],
               ['active'],
               ['guest'],
               ['read_comment'],
               null
           ]
       ];
    }
}